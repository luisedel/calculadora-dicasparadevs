# Usa una imagen de Nginx como base
FROM nginx:latest

# Copia los archivos HTML, CSS y JavaScript a la carpeta de archivos estáticos de Nginx
COPY index.html /usr/share/nginx/html/
COPY styles.css /usr/share/nginx/html/
COPY scripts.js /usr/share/nginx/html/

